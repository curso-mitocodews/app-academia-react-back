package com.gob.pe.service;

import com.gob.pe.model.Usuario;
import com.gob.pe.security.User;

import reactor.core.publisher.Mono;

public interface IUsuarioService extends ICRUD<Usuario, String>{
	
	Mono<User> buscarPorUsuario(String usuario);
}
