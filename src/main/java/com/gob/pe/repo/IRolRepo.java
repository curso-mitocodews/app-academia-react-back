package com.gob.pe.repo;

import com.gob.pe.model.Rol;

public interface IRolRepo extends IGenericRepo<Rol, String>{

}
