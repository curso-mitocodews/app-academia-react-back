package com.gob.pe.repo;

import com.gob.pe.model.Usuario;

import reactor.core.publisher.Mono;

public interface IUsuarioRepo extends IGenericRepo<Usuario, String>{
	
	Mono<Usuario> findOneByUsuario(String usuario);
}
