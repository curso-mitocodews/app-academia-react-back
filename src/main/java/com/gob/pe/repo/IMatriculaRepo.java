package com.gob.pe.repo;

import com.gob.pe.model.Matricula;

public interface IMatriculaRepo extends IGenericRepo<Matricula, String>{
	
}
