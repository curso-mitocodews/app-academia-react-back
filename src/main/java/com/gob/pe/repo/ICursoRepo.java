package com.gob.pe.repo;

import com.gob.pe.model.Curso;

public interface ICursoRepo extends IGenericRepo<Curso, String>{
	
}
