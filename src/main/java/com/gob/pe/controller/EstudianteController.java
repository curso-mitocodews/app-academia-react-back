package com.gob.pe.controller;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;
import static reactor.function.TupleUtils.function;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.util.Map;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils; 
import com.gob.pe.model.Estudiante;
import com.gob.pe.pagination.PageSupport;
import com.gob.pe.service.IEstudianteService;

import net.minidev.json.JSONObject;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {

	@Autowired
	private IEstudianteService service;
	
	@Value("${ruta.subida}")
	private String RUTA_SUBIDA;
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Estudiante>>> listar(){
		Flux<Estudiante> fxEstudiantes = service.listar(); //Flux<Estudiante>
		
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(fxEstudiantes)
				);
	}
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<Estudiante>> listarPorId(@PathVariable("id") String id){
		return service.listarPorId(id) //Mono<Estudiante>
					.map(p -> ResponseEntity.ok() //Mono<ResponseEntity>
							.contentType(MediaType.APPLICATION_JSON)
							.body(p)
					)
					.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@GetMapping("/ordenByDesc")
	public Mono<ResponseEntity<Flux<Estudiante>>> ordenado(){
		Flux<Estudiante> fxEstudiante = service.listar();
		
		Flux<Estudiante> fxEstudiante1 = fxEstudiante.sort( (o1, o2) -> o1.getEdad().compareTo(o2.getEdad()));

				return Mono.just(ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(fxEstudiante1)
						);
	}

	@PostMapping
	public Mono<ResponseEntity<Estudiante>> registrar(@Valid @RequestBody Estudiante estudiante, final ServerHttpRequest req){
		//localhost:8080/estudiantes/123
		return service.registrar(estudiante)
				.map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)
				);
	}
	
	@PutMapping
	public Mono<ResponseEntity<Estudiante>> modificar(@Valid @RequestBody Estudiante estudiante) {	
		return service.modificar(estudiante)
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)
				);
	}
	
	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id){
		return service.listarPorId(id)
				.flatMap(p -> {
					return service.eliminar(p.getId()) //Mono<Void>
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));						
				})
				.defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
					
	}
	
	//private Estudiante estudianteHateoas;
	
	@GetMapping("/hateoas/{id}")
	public Mono<EntityModel<Estudiante>> listarHateoasPorId(@PathVariable("id") String id) {
		Mono<Link> link1 = linkTo(methodOn(EstudianteController.class).listarPorId(id)).withSelfRel().toMono();
		Mono<Link> link2 = linkTo(methodOn(EstudianteController.class).listarPorId(id)).withSelfRel().toMono();
		
		//PRACTICA NO RECOMENDADA
		/*return service.listarPorId(id)
				.flatMap(p -> {
					this.platoHateoas = p;
					return link1;
				}).map(links -> {
					return EntityModel.of(this.platoHateoas, links);
				});*/
		
		//PRACTICA INTEMERDIA
		/*return service.listarPorId(id)
				.flatMap(p -> {
					return link1.map(links -> EntityModel.of(p, links));
				});*/
		
		//PRACTICA IDEAL
		/*return service.listarPorId(id)
				.zipWith(link1, (p, links) -> EntityModel.of(p, links));*/
		
		//Más de 1 link
		return link1.zipWith(link2)
				.map(function((left, right) -> Links.of(left, right)))				
				.zipWith(service.listarPorId(id), (links, p) -> EntityModel.of(p, links));
	}
	
	@PostMapping("/subir/{id}")
	public Mono<ResponseEntity<Estudiante>> subir(@PathVariable String id, @RequestPart FilePart file){
		return service.listarPorId(id)
				.flatMap(c -> {
					c.setUrlFoto(UUID.randomUUID() + "-" + file.filename());
					return file.transferTo(new File(RUTA_SUBIDA + c.getUrlFoto())).then(service.registrar(c));
				})
				.map(c -> ResponseEntity.ok(c))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@PostMapping("subir/cloud/{id}")
	public Mono<ResponseEntity<Estudiante>> subirCloudinary(@PathVariable String id, @RequestPart FilePart file){
		Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap
				("cloud_name", "dn8wtqll3",
				"api_key", "396582371742675",
				"api_secret", "PfXbZrbUUxyYP5s9zp-Wdh2VQfA"));
		
		return service.listarPorId(id)
				.flatMap(c -> {
					String url ="";
					try {						
						File f = Files.createTempFile("temp",file.filename()).toFile();
						file.transferTo(f);
						Map response = cloudinary.uploader().upload(f, ObjectUtils.asMap("resource_types","auto"));
						JSONObject json = new JSONObject(response);
						url =json.get("url").toString();
						c.setUrlFoto(url);
						service.modificar(c).then(Mono.just(ResponseEntity.ok().body(c)));									
					}
					catch (Exception e) {
						e.printStackTrace();
					}					
					return service.modificar(c);
				})
				.map(c -> ResponseEntity.ok(c))
				.defaultIfEmpty(ResponseEntity.notFound().build());
		
		 
	}
	 
	
	
	@GetMapping("/pageable")
	public Mono<ResponseEntity<PageSupport<Estudiante>>> listarPagebale(
			@RequestParam(name = "page", defaultValue = "0") int page,
		    @RequestParam(name = "size", defaultValue = "10") int size
			){
		
		Pageable pageRequest = PageRequest.of(page, size);
		
		return service.listarPage(pageRequest)
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(p)	
						)
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	
	
}
