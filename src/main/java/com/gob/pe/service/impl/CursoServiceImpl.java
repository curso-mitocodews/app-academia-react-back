package com.gob.pe.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.pe.model.Curso;
import com.gob.pe.repo.ICursoRepo;
import com.gob.pe.repo.IGenericRepo;
import com.gob.pe.service.ICursoService;

@Service
public class CursoServiceImpl extends CRUDImpl<Curso, String> implements ICursoService{

	@Autowired
	private ICursoRepo repo;
	
	@Override
	protected IGenericRepo<Curso, String> getRepo() {
		return repo; 
	}

}
