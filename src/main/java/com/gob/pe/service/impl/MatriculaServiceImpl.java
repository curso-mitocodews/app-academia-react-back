package com.gob.pe.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.pe.model.Matricula;
import com.gob.pe.repo.IMatriculaRepo;
import com.gob.pe.repo.IGenericRepo;
import com.gob.pe.service.IMatriculaService;
 

@Service
public class MatriculaServiceImpl extends CRUDImpl<Matricula, String> implements IMatriculaService{

	@Autowired
	private IMatriculaRepo repo;
	
	@Override
	protected IGenericRepo<Matricula, String> getRepo() {
		return repo; 
	}
	
}
