package com.gob.pe.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Document(collection = "cursos")
public class Curso {
	
	@Id
	private String id;

	 
	@ApiModelProperty(value = "Longitud mínima debe ser 2")
	@NotEmpty
	@Field(name = "nombre") 
	private String nombre;
	
	@NotEmpty
	@Field(name = "siglas")
	private String siglas;
		
	@NotNull
	@Field(name = "estado")
	private Boolean estado;

}
