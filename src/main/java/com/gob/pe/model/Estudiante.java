package com.gob.pe.model;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Data;

@Data
@Document(collection = "estudiantes")
public class Estudiante {
	
	@Id
	private String id;

	@NotEmpty
	private String nombres;

	@NotEmpty
	private String apellidos;
	
	@NotEmpty
	private String dni;
	
	@NotEmpty
	private String edad;
	
	@Field(name = "urlFoto")	
	private String urlFoto;

}
