package com.gob.pe.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.pe.model.Estudiante;
import com.gob.pe.repo.IEstudianteRepo;
import com.gob.pe.repo.IGenericRepo;
import com.gob.pe.service.IEstudianteService;

@Service
public class EstudianteServiceImpl extends CRUDImpl<Estudiante, String> implements IEstudianteService{

	@Autowired
	private IEstudianteRepo repo;
	
	@Override
	protected IGenericRepo<Estudiante, String> getRepo() {
		return repo; 
	}

}
