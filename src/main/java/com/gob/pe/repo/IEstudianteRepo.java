package com.gob.pe.repo;

import com.gob.pe.model.Estudiante;

public interface IEstudianteRepo extends IGenericRepo<Estudiante, String>{
	
}
